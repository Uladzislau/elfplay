var gulp = require('gulp'),
    minifyCss = require('gulp-minify-css'),
    sass = require('gulp-sass'),
    watch = require('gulp-watch'),
    spritesmith = require('gulp.spritesmith'),
    livereload = require('gulp-livereload');

gulp.task('default', function () {
    return gulp.src('src/scss/common.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(minifyCss())
        .pipe(gulp.dest('app/css'));
});

gulp.task('gulpDev', function () {
    return gulp.src('src/scss/style.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest('c:/Users/Uladzislau/Dropbox/LH_VladChernyshevich/561_elfplay_ru/html/css'))
//        .pipe(gulp.dest('app/css'))
        .pipe(livereload());
});

gulp.task('sprite', function () {
    var spriteData = gulp.src('src/sprite/*.png').pipe(spritesmith({
        imgName: 'sprite.png',
        cssName: '_sprite.scss'
    }));
    return spriteData.pipe(gulp.dest('src/scss'));
});

gulp.task('watch', function () {
    gulp.watch('src/scss/*.scss', ['default'])
});

gulp.task('watchDev', function () {
    livereload.listen();
    gulp.watch('src/scss/*.scss', ['gulpDev'])
});