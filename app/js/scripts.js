/* Запускаем когда страница готова | trigger when page is ready */
$(document).ready(function(){
    $('.main-slider').slick({
        dots: true,
        arrows: false,
        speed: 500
    });

    $('.product__desc-toggle-link').click(function(){
       $(this).next().slideToggle(200);
    });
});


/* Другие события | optional triggers

$(window).load(function() { // Когда страница полностью загружена
	
});

$(window).resize(function() { // Когда изменили размеры окна браузера
	
});

*/